# Michigan LZ Software Tutorial
Below is a quick introduction to the main software used in LZ (minus LZLama and a few others).

To follow this tutorial, first; start with the powerpoint, then `RunBACCARAT`, then `RunSimulationsAndAnalysis`.


## Running Simulations
There are two primary bits of software which are used to simulate what signals out of LZ will look like.
* Basically a component centric analog response to anything (BACCARAT)
* Detector Electronics Response (DER)

### BACCARAT
BACCARAT is a Geant4 based C++ application which simulates the particle physics. 
It has the LZ geometry built into it and allows you to propagate particles through it and it will simulate the interactions that occur.

There are three executables to BACCARAT (soon to be two); 
* BACCARATExecutable
* BaccRootConverter
* BaccMCTruth

#### BACCARATExecutable
This handles the particle physics propagation.
It is given a macro which contains instructions as to what you want BACCARAT to do; what partcles and material properties.
Depending on the value of `/Bacc/beamOn X` determines the number of events; ie X-many instances of the particles defined in the macro will occur and each of these instances is a separate event.

The output file is currently in a binary format so isn't easy to read or understand, so `BaccRootConveter` is used.

#### BaccRootConverter
The file is output into a ROOT format; essentially a fancy set of histograms.
The best way to understand what this is in the output is to use the ROOT TBrowser and explore the file.

#### BaccMCTruth
This manipulates the output of BACCARATExecutable doing two things;
* Adds S2 optical photon information (optional)
* Consolidates and reduces the MCTruth information into vertices

If the S2 photons haven't been simulated in BACCARATExecutable a probability map will be used in BaccMCTruth for this.
`BaccMCTruth` also manipulated the ROOT file to be appropriate for the `DER`.

### DER
The DER converts PMT hits into an actual electronic response - which will be very similar to what LZ would actually see.

## Running Analysis
There are three bits worth knowing
- LZap
- ALPACA
- FAST

### LZap
First stage -> turns DER output into something more useable, it gives us reduced quantities (pulse and event classifications) which are easier to then do analysis on.

### FAST and ALPACA
These are the two primary analysis tools.
ALPACA is C++ based as is a more traditional approach in particle physics. ALAPCA loops over each event and does user-written functions.
FAST is a python-based alternative which uses yaml scripts to run highly optimised code. The user defines what's in the yaml, but not the underlying way of doing it (there's a larger learning curve to use it properly, but it will be faster to write and run for most RQs).




