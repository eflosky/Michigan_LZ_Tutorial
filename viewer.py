import ROOT
import copy
import matplotlib.pyplot as plt



def get_keynames(self, dir=""):
    self.cd(dir)
    return [key.GetName() for key in ROOT.gDirectory.GetListOfKeys()]


def view_baccarat_contents(rfile):
    
    tfile = ROOT.TFile(rfile)
    tfile.ls()
    events = tfile.Get("DataTree")
    events.Print()
    
    
def simple_baccarat_canvas(rfile, thebranch, hists):
    
    canvas = ROOT.TCanvas("c","c", len(hists) * 500, 500)
    canvas.Divide(len(hists), 1)
    tfile = ROOT.TFile(rfile)
    branch = tfile.Get(thebranch)
    for i, hist in enumerate(hists):
        canvas.cd(i + 1)
        branch.Draw(hist)
    return canvas


ROOT.gStyle.SetPalette(ROOT.kRainBow)
colors = [ROOT.kBlue, ROOT.kRed, ROOT.kGreen, ROOT.kOrange, ROOT.kMagenta, ROOT.kCyan, ROOT.kViolet, ROOT.kPink,
          ROOT.kSpring, ROOT.kTeal]
ROOT.TFile.GetKeyNames = get_keynames